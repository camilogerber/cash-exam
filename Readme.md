# Cash Online Exam

#### ¿Cómo correr el programa?
Estando situado en la carpeta raíz del proyecto deberemos ejecutar el comando `make run`.

#### ¿Cómo correr las pruebas?
Estando situado en la carpeta raíz del proyecto debemos ejecutar el comando `make test`.

#### ¿Cómo probar el proyecto?
Una vez tengamos corriendo el programa deberemos utilizar un cliente HTTP para probar los endpoints.

El programa por defecto estará a la escucha en el puerto `8001`.

#### Acceso a la base de datos
El proyecto cuenta con una base de datos en memoria H2 la cual se carga con datos que están en el archivo `data.sql` en la carpeta `resources`.

Para acceder a la base de datos se debe ingresar a la URL `http://localhost:8001/h2` y en el panel principal completar con los datos:
* **Driver Class:** org.h2.Driver
* **JDBC URL:** jdbc:h2:mem:testdb
* **User Name:** sa
* **Password:**

Por último se debe hacer clic en el botón `Connect`.

#### Documentación de API con SWAGGER

Es posible acceder a la documentación de Swagger ingresando a la URL `http://localhost:8001/swagger-ui.html`

##### USERS
###### Métodos HTTP:
* **Agregar usuario:**  <br>`POST` localhost:8001/users <br> <br>
***Body:*** <br>
_{ <br>
    "email": "user@test.com", <br>
    "firstName": "Guillermo",<br>
    "lastName": "Francella"<br>
}_
<br>

* Listar todos los usuarios: <br>
`GET` localhost:8001/users <br>
<br>

* Buscar un usuario por ID: <br>
`GET` localhost:8001/users/{id} 

<br>

* Eliminar usuario por ID: (Tambien elimina las LOANS que posea): <br>
`DELETE` localhost:8001/users/{id} 

<br>

##### LOANS
###### Métodos HTTP:
* **Listar todas las LOANS por página y tamaño de página:**<br>
`GET` localhost:8001/page=0&size=5

<br>

* **Listar todas las LOANS por ID de usuario:**<br>
`GET` localhost:8001/page=0&size=5&user_id=0 