package com.exam.cash.services.implementations;

import com.exam.cash.entities.Loan;
import com.exam.cash.entities.LoanResponse;
import com.exam.cash.entities.User;
import com.exam.cash.repository.LoanRepositoryImp;
import com.exam.cash.services.interfaces.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoanServiceImpTest {

	@InjectMocks
	private LoanServiceImp loanServiceImp;

	@Mock
	private LoanRepositoryImp loanRepository;

	@Mock
	private UserService userService;

	@Mock
	private User user;

	@Mock
	private List<Loan> loans;


	@Test
	public void shouldRetrieveResponseObjectWithLoans() {
		int page = 0;
		int size = 3;
		long total = 3L;

		when(loanRepository.findAll(any(Pageable.class))).thenReturn(loans);
		when(loanRepository.count()).thenReturn(total);
		LoanResponse response = loanServiceImp.getAllLoans(page, size);

		assertThat(response.getItems()).isEqualTo(loans);
		assertThat(response.getPaging().getPage()).isEqualTo(page);
		assertThat(response.getPaging().getSize()).isEqualTo(size);
		assertThat(response.getPaging().getTotal()).isEqualTo(total);
	}

	@Test
	public void shouldRetrieveResponseObjectWithoutLoans() {
		int page = 0;
		int size = 3;
		long total = 3L;

		when(loanRepository.findAll(any(Pageable.class))).thenReturn(Collections.emptyList());
		when(loanRepository.count()).thenReturn(total);
		LoanResponse response = loanServiceImp.getAllLoans(page, size);

		assertThat(response.getItems()).isEmpty();
		assertThat(response.getPaging().getPage()).isEqualTo(page);
		assertThat(response.getPaging().getSize()).isEqualTo(size);
		assertThat(response.getPaging().getTotal()).isEqualTo(total);
	}

	@Test
	public void shouldReturnNonNullResponseWhenUserHasLoans() {
		String userId = "1234";
		Integer page = 0;
		Integer size = 3;

		when(userService.getUserById(userId)).thenReturn(user);
		when(loanRepository.count()).thenReturn(5L);
		when(user.getId()).thenReturn(userId);
		when(loanRepository.findByUserId(anyString(), any(Pageable.class))).thenReturn(loans);

		LoanResponse response = loanServiceImp.getAllLoansWithUser(page, size, userId);

		assertThat(response).isNotNull();
		assertThat(response.getPaging().getSize()).isEqualTo(size);
		assertThat(response.getPaging().getTotal()).isEqualTo(5L);
		assertThat(response.getPaging().getPage()).isEqualTo(page);
		assertThat(response.getItems()).isEqualTo(loans);
	}

	@Test
	public void shouldCallDeleteUsers() {
		loanServiceImp.removeLoans(loans);
		verify(loanRepository, times(1)).deleteAll(loans);
	}
}