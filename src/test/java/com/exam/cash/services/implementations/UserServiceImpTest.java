package com.exam.cash.services.implementations;

import com.exam.cash.entities.Loan;
import com.exam.cash.entities.User;
import com.exam.cash.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImpTest {

    @InjectMocks
    private UserServiceImp userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private LoanServiceImp loanService;

    @Mock
    private Loan loan;

    @Test
    public void shouldReturnAllUsers(){
        List<User> expected = new ArrayList<>();
        User user1 = new User("123","Juan","Perez","pepe@mail.com",Collections.emptyList());
        User user2 = new User("1233","Julieta","Martinez","pepe1@mail.com",Collections.emptyList());
        User user3 = new User("1434","Ramiro","Fernandez","pepe2@mail.com",Collections.emptyList());
        expected.add(user1);
        expected.add(user2);
        expected.add(user3);

        when(userRepository.findAll()).thenReturn(expected);
        List<User> result = userService.getAllUsers();

        assertFalse(result.isEmpty());
        assertEquals(expected,result);
    }

    @Test
    public void shouldReturnUserWhenFindById(){
        String userId = "1234";
        User user = new User("1234","Juan","Perez","pepito@mail.com",Collections.emptyList());

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        User userFound = userService.getUserById(userId);

        assertFalse(userFound.isEmpty());
    }

    @Test
    public void shouldDeleteUserByIdWithoutLoans(){
        String userId = "1234";
        User user = new User("1234","Juan","Perez","pepito@mail.com",Collections.emptyList());

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        userService.deleteById(userId);

        verify(userRepository, times(1)).delete(user);
        verify(loanService, times(0)).removeLoans(user.getLoans());
    }

    @Test
    public void shouldDeleteUserByIdAndLoans(){
        String userId = "1234";
        List<Loan> loans = new ArrayList<>();
        loans.add(loan);
        User user = new User("1234","Juan","Perez","pepito@mail.com",loans);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        userService.deleteById(userId);

        verify(userRepository, times(1)).delete(user);
        verify(loanService, times(1)).removeLoans(loans);
    }

    @Test
    public void shouldNotDeleteUserWhenDoestExist(){
        String userId = "1234";
        List<Loan> loans = new ArrayList<>();
        loans.add(loan);
        User user = new User("1234","Juan","Perez","pepito@mail.com",loans);

        userService.deleteById(userId);

        verify(userRepository, times(0)).delete(user);
        verify(loanService, times(0)).removeLoans(loans);
    }

    @Test
    public void shouldSaveUser(){
        String userId = "1234";
        List<Loan> loans = new ArrayList<>();
        loans.add(loan);
        User user = new User("1234","Juan","Perez","pepito@mail.com",loans);

        userService.addNewUser(user);

        verify(userRepository,times(1)).save(user);
    }
}