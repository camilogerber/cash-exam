package com.exam.cash.controllers;

import com.exam.cash.repository.LoanRepository;
import com.exam.cash.services.implementations.LoanServiceImp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = LoanController.class)
class LoanControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private LoanServiceImp loanService;

	@MockBean
	private LoanRepository loanRepository;


	@Test
	void shouldCallGetLoansWithoutUserParam() throws Exception {
		Integer page = 0;
		Integer size = 5;

		mockMvc.perform(get("/loans"))
				.andExpect(status().isNotFound());

		verify(loanService, times(1)).getAllLoans(page, size);
	}

	@Test
	void shouldCallGetLoansWithUserParam() throws Exception {
		Integer page = 0;
		Integer size = 5;
		String userId = "123";

		mockMvc.perform(get("/loans")
				.param("user_id", userId))
				.andExpect(status().isNotFound());

		verify(loanService, times(1)).getAllLoansWithUser(page, size,userId);
	}


}