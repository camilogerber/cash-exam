package com.exam.cash.controllers;

import com.exam.cash.entities.User;
import com.exam.cash.services.implementations.UserServiceImp;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserController.class)
class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserServiceImp userService;

	private final List<User> userList = new ArrayList<>();

	@BeforeEach
	void setUp() {

		userList.add(new User("123213", "user1@gmail.com", "pepe", "rodriguez", Collections.emptyList()));
		userList.add(new User("123213", "user1@gmail.com", "sergio", "massa", Collections.emptyList()));
		userList.add(new User("123213", "user1@gmail.com", "mauricio", "macri", Collections.emptyList()));
	}

	@Test
	void shouldFetchAllUsers() throws Exception {
		when(userService.getAllUsers()).thenReturn(userList);

		this.mockMvc.perform(get("/users"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.size()").value(userList.size()));
	}

	@Test
	void shouldReturnNotFoundStateWhenNoUsersFound() throws Exception {
		List<User> users = Collections.emptyList();
		when(userService.getAllUsers()).thenReturn(users);

		this.mockMvc.perform(get("/users"))
				.andExpect(status().isNotFound());
	}

	@Test
	void shouldFetchOneUserById() throws Exception {
		String userId = "1234";
		User user = new User("1234", "pepe@mail.com", "pepe", "gonzalez", Collections.emptyList());
		when(userService.getUserById(userId)).thenReturn(user);

		this.mockMvc.perform(get("/users/{id}", userId))
				.andExpect(status().isOk());
	}

	@Test
	void shouldReturnNotFoundWhenUserNotFound() throws Exception {
		String userId = "1234";
		when(userService.getUserById(userId)).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND));

		this.mockMvc.perform(get("/users/{id}", userId))
				.andExpect(status().isNotFound());
	}

	@Test
	void shouldCreateNewUser() throws Exception {
		User user = new User("1234", "pepe@mail.com", "pepe", "gonzalez", Collections.emptyList());

		this.mockMvc.perform(post("/users")
				.content(asJsonString(user))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	void shouldReturn400WhenCreateNullUser() throws Exception {
		User user = null;

		this.mockMvc.perform(post("/users")
				.content(asJsonString(user))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	void shouldReturn400WhenCreateUserWithoutEmail() throws Exception {
		User user = new User("1234",null,"pepe","rodriguez",Collections.emptyList());

		this.mockMvc.perform(post("/users")
				.content(asJsonString(user))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	void shouldReturn400WhenCreateUserWithoutFirstName() throws Exception {
		User user = new User("1234","pepe@mail.com",null,"rodriguez",Collections.emptyList());

		this.mockMvc.perform(post("/users")
				.content(asJsonString(user))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	void shouldReturn400WhenCreateUserWithoutLastName() throws Exception {
		User user = new User("1234","pepe@mail.com","Pepe",null,Collections.emptyList());

		this.mockMvc.perform(post("/users")
				.content(asJsonString(user))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	void shouldDeleteUser() throws Exception {
		String userId = "1234";
		User user = new User(userId, "pepe@mail.com", "pepe", "gonzalez", Collections.emptyList());
		when(userService.getUserById(userId)).thenReturn(user);

		this.mockMvc.perform(delete("/users/{id}", user.getId()))
				.andExpect(status().isOk());
	}

	@Test
	void shouldReturn400WhenDeleteNonExistentUser() throws Exception {
		String userId = "1234";
		when(userService.getUserById(userId)).thenReturn(new User());
		this.mockMvc.perform(delete("/users/{id}", userId))
				.andExpect(status().isBadRequest());
	}

	private String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}