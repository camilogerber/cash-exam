package com.exam.cash.repository;

import com.exam.cash.entities.Loan;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoanRepositoryImpTest {

	@InjectMocks
	private LoanRepositoryImp loanRepositoryImp;

	@Mock
	private LoanRepository loanRepository;

	@Mock
	private Page<Loan> loans;

	@Mock
	private Pageable pageable;

	@Mock
	private List<Loan> loanList;

	@Test
	public void shouldReturnLoanList() {
		when(loanRepository.findAll(pageable)).thenReturn(loans);
		when(loans.hasContent()).thenReturn(true);
		when(loans.getContent()).thenReturn(loanList);

		List<Loan> result = loanRepositoryImp.findAll(pageable);

		assertThat(result).isEqualTo(loanList);
	}

	@Test
	public void shouldReturnEmptyLoanList() {
		when(loanRepository.findAll(pageable)).thenReturn(loans);
		when(loans.hasContent()).thenReturn(false);

		List<Loan> result = loanRepositoryImp.findAll(pageable);

		assertThat(result).isEqualTo(Collections.EMPTY_LIST);
	}

	@Test
	public void shouldReturnCount() {
		when(loanRepository.count()).thenReturn(3L);

		long count = loanRepositoryImp.count();

		assertThat(count).isEqualTo(3L);
	}

	@Test
	public void shouldReturnLoansByUser(){
		String userId = "123";
		when(loanRepository.findByUserId(userId,pageable)).thenReturn(loans);

		List<Loan> result = loanRepositoryImp.findByUserId(userId,pageable);

//		assertThat(result).isEqualTo(loans);
	}

	@Test
	public void shouldDeleteLoans(){
		loanRepositoryImp.deleteAll(loanList);
		verify(loanRepository,times(1)).deleteAll(loanList);
	}
}