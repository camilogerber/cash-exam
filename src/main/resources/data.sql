insert into USERS (USER_ID,EMAIL,FIRST_NAME,LAST_NAME) values (0,'user1@example.com','user1_name','user1_lastname');
insert into USERS (USER_ID,EMAIL,FIRST_NAME,LAST_NAME) values (1,'user2@example.com','user2_name','user2_lastname');
insert into USERS (USER_ID,EMAIL,FIRST_NAME,LAST_NAME) values (2,'user3@example.com','user3_name','user3_lastname');
insert into USERS (USER_ID,EMAIL,FIRST_NAME,LAST_NAME) values (3,'user4@example.com','user4_name','user4_lastname');
insert into USERS (USER_ID,EMAIL,FIRST_NAME,LAST_NAME) values (4,'user5@example.com','user5_name','user5_lastname');
insert into USERS (USER_ID,EMAIL,FIRST_NAME,LAST_NAME) values (5,'user6@example.com','user6_name','user6_lastname');
insert into USERS (USER_ID,EMAIL,FIRST_NAME,LAST_NAME) values (6,'user7@example.com','user7_name','user7_lastname');

insert into LOANS (ID_LOAN,TOTAL,USER_ID) values (0,5000,0);
insert into LOANS (ID_LOAN,TOTAL,USER_ID) values (1,10000,0);
insert into LOANS (ID_LOAN,TOTAL,USER_ID) values (2,2500,2);
insert into LOANS (ID_LOAN,TOTAL,USER_ID) values (3,3500,3);
insert into LOANS (ID_LOAN,TOTAL,USER_ID) values (4,70000,4);
insert into LOANS (ID_LOAN,TOTAL,USER_ID) values (5,73400,5);
insert into LOANS (ID_LOAN,TOTAL,USER_ID) values (6,23000,6);