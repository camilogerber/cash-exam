package com.exam.cash.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User implements Serializable {

    private static final long serialVersionUID = 6567585080720221731L;

    @Id
    @Column(name = "user_id")
    private String id = UUID.randomUUID().toString();
    @Column(name = "email")
    private String email;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    @OneToMany(mappedBy = "user")
    private List<Loan> loans;

    @JsonIgnore
    public boolean isEmpty(){
        return StringUtils.isEmpty(email) ||
                StringUtils.isEmpty(firstName) ||
                StringUtils.isEmpty(lastName);
    }

    public void addLoan(Loan loan) {
        loans.add(loan);
    }

}
