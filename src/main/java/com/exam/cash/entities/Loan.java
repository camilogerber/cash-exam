package com.exam.cash.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Data
@Table(name = "loans")
@NoArgsConstructor
public class Loan implements Serializable {

	private static final long serialVersionUID = -7550879701188313892L;

	@Id
	@Column(name = "id_loan")
	private String id = UUID.randomUUID().toString();
	@Column(name = "total")
	private float total;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "user_id", nullable = false)
	@JsonIgnore
	private User user;

	public String getUser_id(){
		return user.getId();
	}
}
