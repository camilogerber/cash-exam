package com.exam.cash.controllers;

import com.exam.cash.entities.LoanResponse;
import com.exam.cash.services.interfaces.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/loans")
public class LoanController {

	@Autowired
	private final LoanService loanService;

	public LoanController(LoanService loanService) {
		this.loanService = loanService;
	}

	@GetMapping
	public ResponseEntity<LoanResponse> getLoans(
			@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "5") Integer size,
			@RequestParam(required = false, name = "user_id") String userId) {

		LoanResponse result = (userId != null)
				? loanService.getAllLoansWithUser(page, size, userId)
				: loanService.getAllLoans(page, size);

		return isNullOrEmpty(result) ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(result);
	}

	private boolean isNullOrEmpty(LoanResponse loanResponse) {
		return loanResponse == null || loanResponse.getItems() == null || loanResponse.getItems().isEmpty();
	}
}
