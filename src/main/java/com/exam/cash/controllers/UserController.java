package com.exam.cash.controllers;

import com.exam.cash.entities.User;
import com.exam.cash.services.interfaces.UserService;
import com.sun.istack.NotNull;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<User>> getUsers() throws NotFoundException {
        List<User> users = userService.getAllUsers();
        if (!users.isEmpty()) {
            return ResponseEntity.ok().body(users);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") String id) {
        return ResponseEntity.ok().body(userService.getUserById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteUserById(@PathVariable("id") String id) {
        User user = userService.getUserById(id);
        if(!user.isEmpty()){
            userService.deleteById(id);
            return ResponseEntity.ok().body("User deleted successfully!");
        } else {
            return ResponseEntity.badRequest().body("Bad request: User doesn't exist");
        }

    }

    @PostMapping
    public ResponseEntity<String> addUser(@NotNull @RequestBody User user) {
        if(isValid(user)) {
            userService.addNewUser(user);
            return ResponseEntity.ok().body("User added successfully!");
        } else {
            return ResponseEntity.badRequest().body("Error: Bad request, invalid user");
        }
    }

    private boolean isValid(User user){
        return (user != null
                && user.getFirstName() != null
                && user.getLastName() != null
                && user.getEmail() != null);
    }
}
