package com.exam.cash.repository;

import com.exam.cash.entities.Loan;
import com.exam.cash.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface LoanRepository extends PagingAndSortingRepository<Loan, Serializable> {
    Page<Loan> findByUserId(String userId, Pageable pageable);
}
