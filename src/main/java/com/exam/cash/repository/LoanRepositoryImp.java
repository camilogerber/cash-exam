package com.exam.cash.repository;

import com.exam.cash.entities.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class LoanRepositoryImp {

	private final LoanRepository loanRepository;

	public LoanRepositoryImp(LoanRepository loanRepository) {
		this.loanRepository = loanRepository;
	}

	public List<Loan> findAll(Pageable pageable) {
		Page<Loan> loanPage = loanRepository.findAll(pageable);

		if (loanPage.hasContent()) {
			return loanPage.getContent();
		}
		return Collections.emptyList();
	}

	public List<Loan> findByUserId(String userId, Pageable pageable){
		Page<Loan> loanPage = loanRepository.findByUserId(userId,pageable);
		if(loanPage.hasContent()){
			return loanPage.getContent();
		}
		return Collections.emptyList();
	}

	public long count(){
		return loanRepository.count();
	}

	public void deleteAll(Iterable<? extends Loan> loans){
		loanRepository.deleteAll(loans);
	}

}
