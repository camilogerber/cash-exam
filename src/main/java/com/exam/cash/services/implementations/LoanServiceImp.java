package com.exam.cash.services.implementations;

import com.exam.cash.entities.Loan;
import com.exam.cash.entities.LoanResponse;
import com.exam.cash.entities.Paging;
import com.exam.cash.entities.User;
import com.exam.cash.repository.LoanRepositoryImp;
import com.exam.cash.services.interfaces.LoanService;
import com.exam.cash.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LoanServiceImp implements LoanService {

	@Autowired
	private LoanRepositoryImp loanRepository;
	@Autowired
	private UserService userService;

	@Override
	public void removeLoans(List<Loan> loans) {
		loanRepository.deleteAll(loans);
	}

	@Override
	public LoanResponse getAllLoansWithUser(Integer page, Integer size, String userId) {
		return LoanResponse.builder()
				.items(loanRepository.findByUserId(
						userService.getUserById(userId).getId(),
						PageRequest.of(page, size)))
				.paging(
						new Paging(page, size, loanRepository.count()))
				.build();
	}

	@Override
	public LoanResponse getAllLoans(Integer page, Integer size) {
		return new LoanResponse(
				loanRepository.findAll(PageRequest.of(page, size)),
				new Paging(page, size, loanRepository.count())
		);
	}
}
