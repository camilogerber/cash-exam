package com.exam.cash.services.implementations;

import com.exam.cash.entities.Loan;
import com.exam.cash.entities.User;
import com.exam.cash.repository.UserRepository;
import com.exam.cash.services.interfaces.LoanService;
import com.exam.cash.services.interfaces.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LoanService loanService;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(String id) {
        User user = userRepository.findById(id).orElse(new User());
        if (user.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"entity not found");
        }
        return user;
    }

    @Override
    public void deleteById(String id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            User userFound = user.get();
            if (hasLoans(userFound)) {
                List<Loan> loans = userFound.getLoans();
                loanService.removeLoans(loans);
            }
            userRepository.delete(userFound);
        }
        log.error("User doesn't exist");
    }

    @Override
    public void addNewUser(User user) {
        userRepository.save(user);
    }

    private boolean hasLoans(User userFound) {
        return !userFound.getLoans().isEmpty();
    }
}
