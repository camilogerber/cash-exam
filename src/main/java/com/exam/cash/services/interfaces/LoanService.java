package com.exam.cash.services.interfaces;

import com.exam.cash.entities.Loan;
import com.exam.cash.entities.LoanResponse;

import java.util.List;

public interface LoanService {
    void removeLoans(List<Loan> loans);
    LoanResponse getAllLoansWithUser(Integer page, Integer size, String userId);
    LoanResponse getAllLoans(Integer page, Integer size);
}
