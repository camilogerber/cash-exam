package com.exam.cash.services.interfaces;

import com.exam.cash.entities.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers();
    User getUserById(String id);
    void deleteById(String id);
    void addNewUser(User user);
}
